const location = require('./location');

//find regencies
function getCities(provinceName) {
    const cities = []

    let provinceId = 0;

    // find province where province name equal provinceName
    for(let province of location.provinces) {
        if(province.name == provinceName) {
            provinceId = province.id;
            break;
        }
    }

    // find city where province_id equals provinceId
    for(let regency of location.regencies) {
        if(regency.province_id == provinceId) {
            cities.push(regency.name);
        }
    }

    return cities;
}

//find provinces
function findProvince(cityName) {
        let provName = "";
        let provId;

       // find regency where regency name equal cityName
        for(let regecdncy of location.regencies) {
            if (regency.name == cityName){
              provId = regency.province_id;
              
        
            break;
            }
            
        }
        // find provincy where province_id equals provinceId
        for (let province of location.provinces) {
                if(province.id == provId) {
                   provName = province.name;
                   
                   break;
                }
              
               } 
 return provName;
    
}

function findCityMoreThanNWords(n) {
    let city = [];
    for(let regency of location.regencies) {
        if(regency.name.split(" ").length == n) {
            city.push(regency.name);
        }
    }
    return city;
   
    
}


const nameProvince = "DKI JAKARTA";
const cities = getCities(nameProvince);
console.log(cities)

const nameRegency = "KOTA JAKARTA SELATAN";
const provinsi = findProvince(nameRegency);
console.log(provinsi);
console.log(findCityMoreThanNWords(5));