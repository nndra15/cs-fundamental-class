
function sorting(array) {
  
    let tmpArray = [];
    for (let i = 0; i < array.length; i++)
    {
        for (let j = 0; j < array.length; j++) 
        { 
            if (array[j].umur > array[i].umur) 
            {
                tmpArray = array[j];
                array[j] = array[i];
                array[i] = tmpArray;
            
            }

            else if (array[j].umur ==  array[i].umur)
            {
                if (array[j].name > array[i].name) 
                {
                    tmpArray = array[j];
                    array[j] = array[i];
                    array[i] = tmpArray;
               
                }
            }

        }
    } 

        return array;
  
  }

  const profile = [
    {
        name: "Zeze",
        umur: 30,
    },
    {
        name: "Ali",
        umur: 30,        
    },
    {
        name: "Abdul",
        umur: 10,
    },
    {
        name: "Caca",
        umur: 15,
    },
    {
        name: "Cici",
        umur: 17,
    },
    {
        name: "Cila",
        umur: 17,
    },

    {
        name: "Abdul",
        umur: 15
    }
]

console.log(sorting(profile));